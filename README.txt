/* Vous devez rédiger ce que vous avez compris du sujet liste de courses en 4 à 5 lignes et rappeler le travail que nous avons déjà effectué sur ce sujet */

/* le merge request qui sera retenu (un seul) sera celui qui est le plus clair et le mieux détaillé et sans faute d'orthographe */ 

Le projet de création de l'application de liste de courses doit permettre de gérer des listes de courses.
L'application doit gérer la création, la modification et l'utilisation de ces listes.
Elle devra permettre de créer différentes listes de courses, de pouvoir ajouter ou supprimer des éléments, de pouvoir ajouter un conditionnement ou encore une quantité à un élément de la liste, de pouvoir choisir une enseigne afin de voir le prix de son panier.
Elle devra aussi permettre de créer un groupe avec plusieurs personnes qui pourront partager des listes et qui seront donc modifiables par tous.
Suite à cela, il sera possible de l'imprimer ou encore d'envoyer sa propre liste à quelqu'un.
On pourra aussi trier toutes les listes en fonction des catégories. 
Nous avons déja réalisé sur ce projet un diagramme de cas d'utilisations ainsi qu'une description textuelle afin d'avoir une vision claire du projet.
